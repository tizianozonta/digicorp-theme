<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
    <?php get_template_part('templates/header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>
    <?php get_template_part('templates/footer'); ?>
</body>
</html>
