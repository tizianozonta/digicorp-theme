<?php
// -----------------------------------------------------------------

$theme_includes = [
  'lib/utils.php',
  'lib/config.php',
  'lib/scripts.php',
  'lib/slides.php',
  'lib/products.php',
  'lib/options.php',
  'lib/wp_bootstrap_navwalker.php'
];

foreach ($theme_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'dgc'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);

// -----------------------------------------------------------------
