<?php
// ------------------------------------------------------------------
function dgc_greeting($name = '')
{
    $tag = apply_filters('dgc_filter_greeting_tag', 'h1');
    $message = apply_filters('dgc_filter_greeting_message', 'Hello');

    $pattern = '<%s>%s, %s!</%s>';
    $content = sprintf( $pattern, $tag, $message, $name, $tag );

    do_action( 'dgc_before_greeting', $content);

    //echo htmlspecialchars(apply_filters('dgc_filter_greeting_content', $content));
    echo apply_filters('dgc_filter_greeting_content', $content, $tag, $message, $name);

    do_action( 'dgc_after_greeting', $content);
}
// ------------------------------------------------------------------

// Filters

function change_tag($tag){
    $tag = 'h2';
    return $tag;
}
add_filter('dgc_filter_greeting_tag','change_tag', 2);


function change_tag_again($tag){
    $tag = 'p';
    return $tag;
}
add_filter('dgc_filter_greeting_tag','change_tag_again', 3);


function change_message($message){
    $message = 'Ciao';
    return $message;
}
add_filter('dgc_filter_greeting_message','change_message', 3);


function change_greeting($content, $tag, $message, $name){
    return $content;
}
add_filter('dgc_filter_greeting_content','change_greeting', 10, 4);

// ------------------------------------------------------------------

// Actions

function print_hr($content){
    echo "<hr />";
}
add_action('dgc_before_greeting', 'print_hr');
add_action('dgc_after_greeting', 'print_hr');

function do_something(){
    echo "<h4>do something</h4>";
}
add_action('dgc_after_greeting', 'do_something');

?>