<?php
// -----------------------------------------------------------------

function dgc_theme_setup() {
  register_nav_menu('header-menu', __( 'Header Menu' ));
  register_nav_menu('footer-menu', __( 'Footer Menu' ));

  add_theme_support( 'title-tag' );

  load_theme_textdomain( 'dgc', get_template_directory() . '/languages' );
}
add_action('after_setup_theme', 'dgc_theme_setup' );

// -----------------------------------------------------------------

function dgc_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    $title .= get_bloginfo( 'name' );

    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    if ( $paged >= 2 || $page >= 2 ) {
        $title = sprintf( __( 'Pagina %s', 'dgc' ), max( $paged, $page ) ) . " $sep $title";
    }

    return $title;
}
add_filter( 'wp_title', 'dgc_wp_title', 10, 2 );

// -----------------------------------------------------------------

function dgc_excerpt_length($length) {
    return 20;
}
add_filter('excerpt_length', 'dgc_excerpt_length');

// -----------------------------------------------------------------

function dgc_widgets_init() {
    register_sidebar(array(
        'name' => __( 'Pre Footer', 'dgc' ),
        'id' => 'pre-footer',
        'description' => __( 'I widgets in questa area verrano visualizzati in tutte le pagine', 'dgc' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h1 class="text-xs-center">',
        'after_title'   => '</h1>',
    ));
    register_sidebar(array(
        'name' => __( 'Footer', 'dgc' ),
        'id' => 'footer',
        'description' => __( 'I widgets in questa area verrano visualizzati in tutte le pagine', 'dgc' ),
        'before_widget' => '<div id="%1$s" class="col-sm-4 col-lg-3 m-b-2 widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ));
}
add_action( 'widgets_init', 'dgc_widgets_init' );

// -----------------------------------------------------------------
