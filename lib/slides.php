<?php
// -----------------------------------------------------------------

function dgc_get_slides($limit = 3){
    $args = [
        'post_type' => 'slide',
        'posts_per_page' => $limit,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_status' => 'publish'
    ];

  return get_posts($args);
}

// -----------------------------------------------------------------
