<?php
// -----------------------------------------------------------------

function dgc_get_products($tax_terms_id = [], $limit = 3, $exclude_posts = []){

    $args = [
        'post_type' => 'product',
        'posts_per_page' => $limit,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_status' => 'publish',
    ];

    if(!empty($tax_terms_id)){
        $args['tax_query'] = [
            [
               'taxonomy' => 'section',
                'field' => 'term_id',
                'terms' => $tax_terms_id
            ]
        ];
    }

    if(!empty($exclude_posts)){
        $args['post__not_in'] = $exclude_posts;
    }

    //return get_posts($args);
    $query = new WP_Query($args);
    return $query;

}

// -----------------------------------------------------------------

function dgc_get_in_home_products($limit = 3){
    $args = [
        'post_type' => 'product',
        'posts_per_page' => $limit,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_status' => 'publish',
        'meta_query' => [[
            'key' => 'in_home',
            'value' => '1',
            'compare' => '=='
        ]]
    ];

    $query = new WP_Query($args);
    return $query;
}

// -----------------------------------------------------------------
