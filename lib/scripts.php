<?php
// -----------------------------------------------------------------

function dgc_enqueue_assets() {
    wp_enqueue_style('maincss', get_template_directory_uri() . '/assets/css/main.css', false, '1.0' );
    wp_enqueue_style('customizationcss', get_template_directory_uri() . '/assets/css/customization.css', array('maincss'), '1.0' );
    wp_enqueue_style('fontawesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');

    wp_enqueue_script('tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js');
    wp_enqueue_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js', ['jquery','tether']);
    wp_enqueue_script('mainjs', get_template_directory_uri() . '/assets/js/main.js', ['jquery']);
}

add_action( 'wp_enqueue_scripts', 'dgc_enqueue_assets' );

// -----------------------------------------------------------------
