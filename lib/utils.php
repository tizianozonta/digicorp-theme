<?php
// -----------------------------------------------------------------

function debug($data) {
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

// -----------------------------------------------------------------

function dgc_get_field($field, $post_id = null){
    // check if ACF pluging is enabled
    if(function_exists('get_field')){
        return get_field($field, $post_id);
    }
    return false;
}

// -----------------------------------------------------------------

function dgc_get_feature_image_url($post_id, $size = 'thumbnail') {
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);
    $url = (isset($image[0])) ? $image[0] : '#';
    return $url;
}

// -----------------------------------------------------------------

function dgc_get_terms_ids($terms){
    $ids = array_map(function($term){
        return $term->term_id;
    }, $terms);

    return $ids;
}

// -----------------------------------------------------------------

function get_current_page_url(){
    global $wp;
    return home_url(add_query_arg(array(),$wp->request));
}

// -----------------------------------------------------------------

function is_current_page_url($url){
    $url = trim($url, '/');
    return get_current_page_url() == $url;
}

// -----------------------------------------------------------------
