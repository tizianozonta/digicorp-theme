<div class="jumbotron jumbotron-fluid m-b-3">
    <div class="container">
        <h1 class="display-3"><?php the_title(); ?></h1>
        <?php $slogan = dgc_get_field('page_slogan'); ?>
        <?php if($slogan !== false): ?>
            <p class="lead"><?php echo $slogan; ?></p>
        <?php endif ?>
    </div>
</div>