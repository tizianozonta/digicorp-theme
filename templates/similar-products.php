<?php $terms_ids = dgc_get_terms_ids(wp_get_post_terms($post->ID, 'section')); ?>
<?php $similarProducts = dgc_get_products($terms_ids, 3, [$post->ID]); ?>
<?php if($similarProducts->have_posts()): ?>
<hr class="m-y-3">
<div class="row m-t-3">
  <div class="col-lg-3 col-md-12 m-b-2">
    <h4>Similar products</h4>
    <p class="lead">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
    <a class="btn btn-primary btn-sm" href="<?php echo get_term_link($terms_ids[0]); ?>">View all</a>
  </div>
  <?php while ( $similarProducts->have_posts() ) : $similarProducts->the_post(); ?>
  <div class="col-lg-3 col-md-4">
    <div class="card">
      <a href="<?php the_permalink() ?>">
      <?php if ( has_post_thumbnail() ): ?>
      <?php the_post_thumbnail('full', ['class'=> 'card-img-top img-fluid']); ?>
      <?php endif; ?>
      </a>
      <div class="card-block">
        <h5 class="card-title"><?php the_title() ?></h5>
        <p class="card-text small"><?php the_excerpt(); ?></p>
        <a class="btn btn-primary btn-sm" href="<?php the_permalink() ?>">Read more</a>
      </div>
    </div>
  </div>
  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
</div>
<?php endif; ?>