<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('templates/jumbotron'); ?>

    <section class="container m-b-3">
      <div class="row">
        <div class="col-md-3 col-lg-6 col-xl-5">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 m-b-1">
            <?php if ( has_post_thumbnail() ): ?>
            <?php the_post_thumbnail('full', ['class'=> 'img-fluid']); ?>
            <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-lg-6 col-xl-7">
          <h2><?php echo number_format(dgc_get_field('price'),2); ?> €</h2>
          <h5 class="text-primary m-t-2">Description</h5>
          <div><?php the_content(); ?></div>
          <h5 class="text-primary m-t-2">Dimensions</h5>
          <table class="table table-sm">
          <?php $height = dgc_get_field('height'); ?>
          <?php if (!empty($height)): ?>
            <tr>
              <th scope="row">Height</th>
              <td><?php echo $height ?> cm</td>
            </tr>
          <?php endif ?>
          <?php $width = dgc_get_field('width'); ?>
          <?php if (!empty($width)): ?>
            <tr>
              <th scope="row">Weight</th>
              <td><?php echo $width ?> cm</td>
            </tr>
          <?php endif ?>
          <?php $deepth = dgc_get_field('deepth'); ?>
          <?php if (!empty($deepth)): ?>
            <tr>
              <th scope="row">Deepth</th>
              <td><?php echo $deepth ?> cm</td>
            </tr>
          <?php endif ?>
          <?php $weight = dgc_get_field('weight'); ?>
          <?php if (!empty($weight)): ?>
            <tr>
              <th scope="row">Weight</th>
              <td><?php echo $weight ?> kg</td>
            </tr>
          <?php endif ?>
          </table>
          <?php if(wpba_attachments_exist()): ?>
          <h5 class="text-primary m-t-2">Downloads</h5>
          <ul class="list-unstyled">
            <?php $attachments = wpba_get_attachments(); ?>
            <?php foreach ($attachments as $key => $attachment): ?>
            <li>
              <span class="text-primary fa fa-file-pdf-o"></span>
              <a class="text-muted" href="<?php echo $attachment->guid; ?>"><?php echo $attachment->post_title ?></a>
            </li>
            <?php endforeach ?>
          </ul>
          <?php endif; ?>
        </div>
      </div>

      <?php get_template_part('templates/similar-products'); ?>

    </section>

    <?php get_template_part('templates/slider'); ?>

<?php endwhile; else : ?>
<?php get_template_part('templates/not-found'); ?>
<?php endif; ?>