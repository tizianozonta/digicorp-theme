<div class="card">
  <?php if ( has_post_thumbnail() ): ?>
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail('full', ['class'=> 'card-img-top img-fluid']); ?>
  </a>
  <?php endif; ?>
  <div class="card-block">
    <h4 class="card-title"><?php the_title() ?></h4>
    <p class="card-text"><?php echo the_excerpt(); ?></p>
    <a class="btn btn-primary btn-sm" href="<?php the_permalink(); ?>"><?php _e('Read more', 'dgc'); ?></a>
  </div>
</div>