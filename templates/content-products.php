<?php if(have_posts()): ?>
<?php
  $title = 'Products';
  if(is_tax()){
    $current_term_slug = get_query_var('section');
    $term = get_term_by( 'slug', $current_term_slug, 'section');
    $title = $term->name;
  }
?>
<section class="jumbotron jumbotron-fluid m-b-3">
  <div class="container">
    <h1 class="display-3"><?php echo $title; ?></h1>
  </div>
</section>
<section class="container m-b-3">
  <div class="card-columns">
    <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part('templates/card'); ?>
    <?php endwhile; ?>
  </div>
</section>
<?php else: ?>
  <?php get_template_part('templates/not-found'); ?>
<?php endif; ?>