<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>

  <br><br>

  <section class="container m-b-3">
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <div><?php the_content(); ?></div>
  </section>

<?php endwhile; else : ?>
<?php get_template_part('templates/not-found'); ?>
<?php endif; ?>
