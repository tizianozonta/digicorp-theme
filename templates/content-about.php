<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('templates/jumbotron'); ?>

    <?php if (!empty($post->post_content)): ?>
    <section class="container m-b-3">
        <?php the_content(); ?>
    </section>
    <?php endif; ?>

    <section class="container m-b-3">
        <?php
            $titolo_1 = dgc_get_field('titolo_sezione_1');
            $testo_1 = dgc_get_field('testo_sezione_1');
            $img_1 = dgc_get_field('immagine_sezione_1');
        ?>
        <?php if (!empty($titolo_1) && !empty($testo_1)): ?>
        <div class="row p-y-3 row-md-center">
            <div class="col-md-6 col-xl-7">
                <h1><?php echo $titolo_1 ?></h1>
                <div><?php echo $testo_1 ?></div>
            </div>
            <?php if($img_1): ?>
            <div class="col-md-6 col-xl-5">
                <img alt="" class="img-fluid" src="<?php echo $img_1['url']; ?>">
            </div>
            <?php endif; ?>
        </div>
        <?php endif ?>
        <?php
            $titolo_2 = dgc_get_field('titolo_sezione_2');
            $testo_2 = dgc_get_field('testo_sezione_2');
            $img_2 = dgc_get_field('immagine_sezione_2');
        ?>
        <?php if (!empty($titolo_2) && !empty($testo_2)): ?>
        <hr class="m-y-3">
        <div class="row p-y-3 row-md-center">
            <div class="col-md-6 col-xl-7 col-md-last">
                <h1><?php echo dgc_get_field('titolo_sezione_2') ?></h1>
                <div><?php echo dgc_get_field('testo_sezione_2') ?></div>
            </div>
            <?php if($img_2): ?>
            <div class="col-md-6 col-xl-5">
                <img alt="" class="img-fluid" src="<?php echo $img_2['url']; ?>">
            </div>
            <?php endif; ?>
        </div>
        <?php endif ?>
        <?php
            $titolo_3 = dgc_get_field('titolo_sezione_3');
            $testo_3 = dgc_get_field('testo_sezione_3');
            $img_3 = dgc_get_field('immagine_sezione_3');
        ?>
        <?php if (!empty($titolo_3) && !empty($testo_3)): ?>
        <hr class="m-y-3">
        <div class="row p-y-3 row-md-center">
            <div class="col-md-6 col-xl-7">
                <h1><?php echo dgc_get_field('titolo_sezione_3') ?></h1>
                <div><?php echo dgc_get_field('testo_sezione_3') ?></div>
            </div>
            <?php if($img_3): ?>
            <div class="col-md-6 col-xl-5">
                <img alt="" class="img-fluid" src="<?php echo $img_3['url']; ?>">
            </div>
            <?php endif; ?>
        </div>
        <?php endif ?>
    </section>

<?php endwhile; else : ?>
<?php get_template_part('templates/not-found'); ?>
<?php endif; ?>