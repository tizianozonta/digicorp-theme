<header class="header">
  <div class="collapse bg-faded" id="navbar-header">
    <iframe allowfullscreen="" class="center-block" height="400" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.196516519392!2d-0.15607444795285458!3d51.509610579535526!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487605321dfbaacd%3A0x733a5a71063b7386!2s42+Park+St%2C+Mayfair%2C+London+W1K+2JQ%2C+Regno+Unito!5e0!3m2!1sit!2sus!4v1455034515860" width="1600"></iframe>
  </div>
  <nav class='navbar navbar-static-top navbar-full navbar-dark bg-inverse'>
    <div class="container">
      <?php
        if (has_nav_menu('header-menu')){
          wp_nav_menu([
            'theme_location' => 'header-menu',
            'menu'   => false,
            'menu_class' => 'nav navbar-nav',
            'walker' => new wp_bootstrap_navwalker()
          ]);
        }
      ?>
      <ul class="nav navbar-nav pull-xs-right">
        <li class="nav-item">
            <a class="nav-link" href="#">
              <span class="fa fa-facebook"></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span class="fa fa-twitter"></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-target="#navbar-header" data-toggle="collapse" href="#">
              <span class="fa fa-map-marker"></span><?php _e('Map', 'dgc'); ?>
            </a>
          </li>
        </ul>
      </div>
    </nav>
</header>
