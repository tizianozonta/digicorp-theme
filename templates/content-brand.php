<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('templates/jumbotron'); ?>

    <section class="container m-b-3">
        <?php the_content(); ?>

        <?php $products_in_home = dgc_get_in_home_products(); ?>
        <?php if ($products_in_home->have_posts()): ?>
        <div class="row">
            <?php while ( $products_in_home->have_posts() ) : $products_in_home->the_post(); ?>
            <div class="col-md-4">
            <?php get_template_part('templates/card'); ?>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
        <?php endif ?>

    </section>

    <?php get_template_part('templates/slider'); ?>

<?php endwhile; else : ?>
<?php get_template_part('templates/not-found'); ?>
<?php endif; ?>