<?php $dgc_footer_options = get_option( 'digicorp_theme_option_name'); ?>

<div class="bg-primary p-y-3">
  <div class="container">
      <?php
        if ( is_active_sidebar( 'pre-footer' ) ){
          dynamic_sidebar( 'pre-footer' );
        }
      ?>
  </div>

</div>
<div class="bg-inverse p-y-3">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-lg-6 m-b-2">
        <div class="media">
          <a class="media-left hidden-md-down" href="#">
            <img alt="" class="media-object" src="https://unsplash.it/150?image=763">
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?php echo bloginfo('name' ); ?></h4>
            <p><?php echo htmlspecialchars_decode($dgc_footer_options['footer_message_3']) ?></p>
          </div>
        </div>
      </div>
      <?php
        if ( is_active_sidebar( 'footer' ) ){
          dynamic_sidebar( 'footer' );
        }
      ?>
    </div>
  </div>
</div>
<?php $footer_menu_items = wp_get_nav_menu_items('Footer'); ?>
<div class="bg-faded">
  <div class="container">
  <?php if(!empty($footer_menu_items)): ?>
    <ul class="list-inline text-sm-right small m-b-0 p-y-1">
    <?php foreach ($footer_menu_items as $item): ?>
      <?php $active_class = (is_current_page_url($item->url)) ? ' active' : ''; ?>
      <li class="list-inline-item<?php echo $active_class; ?>">
        <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
      </li>
    <?php endforeach ?>
    </ul>
  <?php endif; ?>
  </div>
</div>

<?php wp_footer(); ?>
