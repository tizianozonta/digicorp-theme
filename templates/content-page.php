<?php if (have_posts()) : while ( have_posts() ) : the_post(); ?>

  <?php get_template_part('templates/jumbotron'); ?>

  <section class="container m-b-3">
    <?php the_content(); ?>
  </section>

<?php endwhile; else : ?>
    <?php get_template_part('templates/not-found'); ?>
<?php endif; ?>
