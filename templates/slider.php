<?php
  $slides = dgc_get_slides();
  $slidesCount = count($slides);
?>

<?php if ($slidesCount > 0): ?>
<section class="container-fluid p-x-0">
  <div class="carousel slide" data-ride="carousel" id="carousel-example-generic">
    <ol class="carousel-indicators">
      <?php for ($i = 0; $i < $slidesCount; $i++) : ?>
      <li class="<?php echo ($i === 0) ? 'active' : ''?>" data-slide-to="<?php echo $i; ?>" data-target="#carousel-example-generic"></li>
      <?php endfor;?>
    </ol>
    <div class="carousel-inner" role="listbox">
    <?php foreach ($slides as $key => $slide): ?>
      <div class="carousel-item <?php echo ($key === 0) ? 'active' : '';?>">
        <img alt="<?php echo $slide->post_title ?>" src="<?php echo dgc_get_feature_image_url($slide->ID, 'orginal');?>">
        <div class="carousel-caption">
          <h3 class="m-b-0"><?php echo $slide->post_title ?></h3>
          <p><?php echo $slide->post_content ?></p>
        </div>
      </div>
    <?php endforeach ?>
    <?php if ($slidesCount > 1): ?>
    <a class="left carousel-control" data-slide="prev" href="#carousel-example-generic" role="button">
      <span aria-hidden="true" class="icon-prev"></span>
      <span class="sr-only"><?php _e('Previous', 'dgc') ?></span>
    </a>
    <a class="right carousel-control" data-slide="next" href="#carousel-example-generic" role="button">
      <span aria-hidden="true" class="icon-next"></span>
      <span class="sr-only"><?php _e('Next', 'dgc') ?></span>
    </a>
    <?php endif ?>
  </div>
</section>
<?php endif ?>
