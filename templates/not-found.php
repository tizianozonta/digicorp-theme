<div class="jumbotron jumbotron-fluid m-b-3">
    <div class="container">
        <h1 class="display-3"><?php wp_title( '|', true, 'right' ); ?></h1>
    </div>
</div>
<section class="container m-b-3">
    <h3><?php _e( 'Spiecente non ci sono contenuti per questa pagina.', 'dgc'); ?></h3>
</section>